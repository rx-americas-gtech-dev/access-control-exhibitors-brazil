
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css" crossorigin="anonymous">


    <title>CONTROLE DE ACESSO DOS VISITANTES</title>
  </head>
  <body>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 heaader">
                    <img src="img/logo.jpg" class="img-fluid" alt="Responsive image">
                </div>

                <div class="col-lg-12 acess">
                    <h2>CONTROLE DE ACESSO VISITANTES</h2>
                    <h3><?php echo base64_decode($nomeFeira); ?> </h3>
                    <form>
                        <div class="form-group">
                            <input autofocus type="number" class="form-control" id="credencial" placeholder='Credencial'>
                            <input type="hidden" name="idFeira" id="idFeira" value="<?=base64_decode($idFeira)?>" />
                        </div>

                        <div class="alert alert-success d-none" id="sucesso" role="alert">
                        
                        
                        </div>
            
                        <div class="alert alert-danger d-none" style="background-color:red;" role="alert" id='erro'>
                            CREDENCIAL N�O ENCONTRADA!<br> 
                        </div>
                    
                    </form>
                </div>
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-3.3.1.slim.min.js" ></script>
        <script src="js/jquery-3.4.0.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="js/bootstrap.min.js" ></script>
        <script>
            $(function() {

                var enviaCredencial = (function(credencial,feira){
                    var urlAjax =  "ajax_grava_checkin.php?c=" + credencial+ "&f=" +feira;
                    console.log(urlAjax);
                    $.ajax({

                        type: "GET",
                        url: urlAjax,
                        dataType: "html",
                        success: function(data) {
                            console.log(data);
                            var resultVet = data.split("_");
                            if(resultVet[0]==2){
                                $( "#sucesso" ).addClass( "d-none");
                                $( "#erro" ).removeClass( "d-none");
                                $("#erro").append("<strong>"+resultVet[1]+"</strong><br><strong style='color:#000;'> ACESSO N�O PERMITIDO </strong>");
                                $("#credencial").val("");  
                            }
                            if(resultVet[0]==1){
                                $( "#erro" ).addClass( "d-none");
                                $( "#sucesso" ).removeClass( "d-none");
                                $("#sucesso").append(resultVet[1]);
                                $("#credencial").val("");  
                            }
                        }
                    });        
                })
                
                //CONSULTA CREDENCIAL
                $("#credencial").on("keypress",function(event){
                    if(event.which == 13) {
                        //cancela a a��o padr�o
                        event.preventDefault();
                        
                        var credencial  = $("#credencial").val();
                        var idFeira     = $("#idFeira").val();

                        if($(this).val().length !==0 ){
                            $('#sucesso').html("");
                            $('#erro').html("");
                            enviaCredencial(credencial,idFeira); 
                                    
                        }else{
                            $('#sucesso').html("");
                            $('#erro').html("");
                            $( "#sucesso" ).addClass( "d-none");
                            $( "#erro" ).removeClass( "d-none");
                            $("#erro").append("<strong>ESTA CREDENCIAL N�O � V�LIDA!<br>ACESSO N�O PERMITIDO</strong>");
                            $("#credencial").val("");  
                        }
                    }
                
                });
            });
        </script>
  </body>
</html>