<?php
	header ('Content-type: text/html; charset=iso-8859-1');

    /*
        P�GINA PARA CHECK-IN DE VISITANTES RECEBE CONSULTA AJAX
        ALTERADA POR CLEYTON DIA 15-04-2019
    */
    //ini_set("display_errors", 1);
    error_reporting(E_ALL);


    include_once('include_CONFIG.PHP'); //VARIAVEIS GLOBAIS E CONEXAO
    include_once('functions_checkin.php');

    //sleep(5);

    mysqli_set_charset($GLOBALS['my'],"latin1");

    $credencial = $_GET["c"];
    $idFeira   = $_GET["f"];

    $idFeiracheck = search_linkedEvents($idFeira,$credencial);

    $query  = "SELECT
	C.id,
    C.Tipo,
    T.descricao AS tipoCredencial,
    C.Nome,
    C.Cargo,
    C.rg,
    C.status,
    C.idExpositor,
    EC.nmFantasia AS expositor,
EC.rxam_expositor AS idE,
 	C.idMontador,
    CL.nmFantasia AS montador,
    C.idPedidoServico,
    C.idFeira,
    C.gestorOrigem,
CL.status AS ativo
FROM
	rxam_credenciais C
LEFT JOIN rxam_credenciaisTipo T ON (T.tipo = C.tipo) LEFT JOIN expositorcatalogo EC ON (EC.id = C.idExpositor) LEFT JOIN rxam_cliente CL ON (CL.id = C.idMontador) WHERE
	C.idFeira = '".$idFeiracheck."'
	AND C.id = '".$credencial."'";


if($result = mysqli_query($GLOBALS['my'],$query)){

    $row = $result->fetch_array(MYSQLI_ASSOC);
    $status             = $row["status"]; // 0 - cancelada 1- V�lida
    $gestorOrigem       = $row["gestorOrigem"];// 1 - Verifica apenas status 0 - Verifica se foi pago na tabela de boletos rxam_boleto
    $idMontador         = $row["idMontador"]; // 0 - Credencial de Expositor   INT - Outro n�mero credencial de montador
    $idPedidoServico    = $row["idPedidoServico"]; // Se existir esse ID  buscar  na tabela de boletos rxam_boleto Status = P
    $ativo              = $row["ativo"]; // Caso o cara seja montador devo verificar esse status
    $tipoCredencial     = $row["tipoCredencial"];
    $Nome               = $row["Nome"];
    $Cargo              = $row["Cargo"];
    $idE                = $row["idE"];

    //FLUXO 1 - ORIGEM GESTOR 
    //VALIDO SE � EXPOSITOR OU MONTADOR
    //VALIDAR ORIGEM
    //SE FOR GESTOR VALIDAR STATUS
    //VERIFICO SE TEM IMAGEM
   
    if ($status ==1){//STATUS CREDENCIAL
        if ($gestorOrigem==1){ //CREDENCIAL PERSONALIZADA

            echo $tipoCredencial."<br>".$Nome. " <br><strong>LIBERADO</strong><br>";
            //INSERIR O CHECKIN NO BANCO DE DADOS -- LEMBRAR
            $query = "INSERT INTO ".$credenciaischeckin."
            (
            idFeira,
            idCredencial,
            dtCheckin,
            status,
            validado
            )
            VALUES(
            ".$idFeiracheck.",
            ".$credencial.",
            NOW(),
            0,
            1)
            ";

            $result = mysqli_query($GLOBALS['my'],$query);

        }else{//CASO NAO SEJA PERSONALIZADA VAMOS AS OUTRS VERIFICA��ES
           if($idMontador==0 || $idMontador=="" || $idMontador=NULL){//VERIFICO SE � EXPOSITOR OU MONTADOR
                //CASO SEJA EXPOSITOR VERIFICO CONTRATO E PENDENCIAS
                $query  = "SELECT
                status AS contrato,
                pendencia
                FROM
                rxam_expositor
                WHERE
                id = ".$idE."
                ";
                if($result = mysqli_query($GLOBALS['my'],$query)){
                    $row        = $result->fetch_array(MYSQLI_ASSOC);
                    $contrato   = $row["contrato"]; 
                    $pendencia  = $row["pendencia"];

                 
                    if($contrato==1){
                        if($pendencia==1){
                            echo "5";// PEND�NCIAS FINANCEIRAS
                        }else{
                            echo $tipoCredencial."<br>".$Nome. " <br><strong>LIBERADO</strong><br>";
                            //INSERIR O CHECKIN NO BANCO DE DADOS -- LEMBRAR
                            $query = "INSERT INTO ".$credenciaischeckin."
                            (
                            idFeira,
                            idCredencial,
                            dtCheckin,
                            status,
                            validado
                            )
                            VALUES(
                            ".$idFeiracheck.",
                           ".$credencial.",
                            NOW(),
                           0,
                           1)
                            ";
                
                            $result = mysqli_query($GLOBALS['my'],$query);
                
                        }

                    }else{
                        echo "4"; //SEM CONTRATO ATIVO
                    }
                }else if($gestorOrigem!==""){ //VERIFICO SE � UMA CREDENCIAL PERSONALIZADA
                    echo $tipoCredencial."<br>".$Nome. " <br><strong>LIBERADO</strong><br>";
                    //INSERIR O CHECKIN NO BANCO DE DADOS -- LEMBRAR
                    $query = "INSERT INTO ".$credenciaischeckin."
                    (
                    idFeira,
                    idCredencial,
                    dtCheckin,
                    status,
                    validado
                    )
                    VALUES(
                    ".$idFeiracheck.",
                   ".$credencial.",
                    NOW(),
                   0,
                   1)
                    ";
                    $result = mysqli_query($GLOBALS['my'],$query);
                  
                }else{
                   
                    echo "3";//N�O FOI POSS�VEL CONSULTAR PEND�NCIAS DIRIJA-SE AO CAEX
                }


            

           }else{//CASO SEJA MONTADOR VERIFICAR ABAIXO

                $query  = "SELECT * FROM zope.rxam_credenciais where id=".$credencial;

                if($result = mysqli_query($GLOBALS['my'],$query)){

                    $row                    = $result->fetch_array(MYSQLI_ASSOC);
                    $ativo                  = $row["status"];
                    $idPedidoServico        = $row["idPedidoServico"];

                    if($ativo ==1){ //VERIFICO SE O MONTADOR EST� ATIVO
                        if($idPedidoServico==""){ //LIBERAR
                            echo $tipoCredencial."<br>".$Nome. " <br><strong>LIBERADO</strong><br>";
                            //INSERIR O CHECKIN NO BANCO DE DADOS -- LEMBRAR
                            $query = "INSERT INTO ".$credenciaischeckin."
                            (
                            idFeira,
                            idCredencial,
                            dtCheckin,
                            status,
                            validado
                            )
                            VALUES(
                            ".$idFeiracheck.",
                           ".$credencial.",
                            NOW(),
                           0,
                           1)
                            ";
                
                            $result = mysqli_query($GLOBALS['my'],$query);
    
                        }else{//VALIDAR PAGAMENTO
                            $query="SELECT * FROM zope.rxam_boleto Where id=".$idPedidoServico;
    
                            if($result = mysqli_query($GLOBALS['my'],$query)){
                                $row        = $result->fetch_array(MYSQLI_ASSOC);
                                $statusP   = $row["statusPagamento"]; 
    
                                if($statusP =='P'){//VERIFICO O STATUS
                                    echo $tipoCredencial."<br>".$Nome. " <br><strong>LIBERADO</strong><br>";

                                    $query = "INSERT INTO ".$credenciaischeckin."
                                    (
                                    idFeira,
                                    idCredencial,
                                    dtCheckin,
                                    status,
                                    validado
                                    )
                                    VALUES(
                                    ".$idFeiracheck.",
                                   ".$credencial.",
                                    NOW(),
                                   0,
                                   1)
                                    ";
                        
                                    $result = mysqli_query($GLOBALS['my'],$query);
                                }else{
                                    echo "8"; //PENDENCIA FINANCEIRA
                                }
    
    
                            }else{
                                echo "7";//PEDIDO NAO ENCONTRADO CONSULTE CAEX
                            }
                        }
                    }else{
                        echo "6"; //MONTADOR DESATIVADO
                    }
                    
                }else{
                    echo "9"; //MONTADOR N�O ENCONTRADO
                }

           }
        }

    }else{

        echo "2"; //CREDENCIAL CANCELADA
    
    }





}else{
    echo "1"; //CREDENCIAL N�O ENCONTRADA
}
   

   ?>