<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="css/custom.css" crossorigin="anonymous">


    <script>
        function loadCamera() {
            //Captura elemento de v�deo
            var video = document.querySelector("#webCamera");
            //As op��es abaixo s�o necess�rias para o funcionamento correto no iOS
            video.setAttribute('autoplay', '');
            video.setAttribute('muted', '');
            video.setAttribute('playsinline', '');
            //--
    
            //Verifica se o navegador pode capturar m�dia
            if (navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia({
                        audio: false,
                        video: {
                            facingMode: 'user'
                        }
                    })
                    .then(function(stream) {
                        //Definir o elemento v�deo a carregar o capturado pela webcam
                        video.srcObject = stream;
                    })
                    .catch(function(error) {
                        alert("Oooopps... Falhou :'(");
                    });
            }
        }


        function takeSnapShot() {
            //Captura elemento de v�deo
            var video = document.querySelector("#webCamera");

            //Criando um canvas que vai guardar a imagem temporariamente
            var canvas = document.createElement('canvas');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            var ctx = canvas.getContext('2d');

            //Desenhando e convertendo as dimens�es
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

            //Criando o JPG
            var dataURI = canvas.toDataURL('image/jpeg'); //O resultado � um BASE64 de uma imagem.
            document.querySelector("#base_img").value = dataURI;
            //document.getElementById("image-div").classList.add("d-none");
            sendSnapShot(dataURI); //Gerar Imagem e Salvar Caminho no Banco
        }

        function takeSnapShot() {
            document.querySelector("#imagemConvertida").setAttribute("src", "");
            //Captura elemento de v�deo
            var video = document.querySelector("#webCamera");

            //Criando um canvas que vai guardar a imagem temporariamente
            var canvas = document.createElement('canvas');
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            var ctx = canvas.getContext('2d');

            //Desenhando e convertendo as dimens�es
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

            //Criando o JPG
            var dataURI = canvas.toDataURL('image/jpeg'); //O resultado � um BASE64 de uma imagem.
            var credencial_img = document.getElementById("credencial_img").value;

            document.querySelector("#base_img").value = dataURI;

            document.getElementById("credencial").focus();

            sendSnapShot(dataURI, credencial_img); //Gerar Imagem e Salvar Caminho no Banco
        }

        function sendSnapShot(base64, credencial_img) {
            var request = new XMLHttpRequest();
            request.open('POST', 'ajax_upload_image.php', true);
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            request.onload = function() {
                console.log(request);
                if (request.status >= 200 && request.status < 400) {
                    //Colocar o caminho da imagem no SRC
                    var data = JSON.parse(request.responseText);

                    //verificar se houve erro
                    if (data.error) {
                        alert(data.error);
                        return false;
                    }

                    //Mostrar informa��es

                    document.querySelector("#imagemConvertida").setAttribute("src", data.img);
                    location.reload();
                    //document.querySelector("#caminhoImagem a").setAttribute("href", data.img);
                    //document.querySelector("#caminhoImagem a").innerHTML = data.img.split("/")[1];
                } else {
                    alert("Erro ao salvar. Tipo:" + request.status);
                }
            };

            request.onerror = function() {
                alert("Erro ao salvar. Back-End inacess�vel.");
            }
            var idFeira = document.getElementById("idFeira").value;
            request.send("base_img=" + base64 + "&credencial_img=" + idFeira + "_" + credencial_img); // Enviar dados</code></pre>

        }
    </script>

    <title>CONTROLE DE ACESSO</title>
</head>

<body>
    <script>
        function tecla() {
            var teclaFoto = event.keyCode;
            if (teclaFoto == 92 || teclaFoto == 113) {
                takeSnapShot();
            }
            // window.alert("O c�digo da tecla pressionada foi: " + event.keyCode);

        }

        document.body.onkeypress = tecla;
    </script>
    <div class="header">
        <img src="img/header-controle-acesso.png" class="img-fluid" alt="RX BRASIL">
    </div>
    <div class="subheader" style="display: none;">
        <h4>EXPOSITOR </h4>
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12 titulo">
                <h2>EXPOSITOR & PRESTADORES DE SERVI�O <span class="bolinha">&bull;</span> <span class="nmfeira"><?php echo base64_decode($nomeFeira); ?></span></h2>
                <form>
                    <div class="form-row" style="margin-top: 30px;">
                        <div class="form-group col-md-12">
                            <input autofocus type="number" class="form-control form-control-lg" id="credencial" placeholder='Credencial'>
                            <input type="text" name="idFeira" id="idFeira" value="<?= base64_decode($idFeira) ?>" class='d-none' />
                        </div>

                        <div id="carregando" style="display: none;">
                            <div class="spinner-border text-success" role="status">
                                <span class="sr-only"></span>
                            </div> <span class="textocarregando">Buscando...</span>
                        </div>

                        <div class="col-md-3 d-none" id='image-div'>
                            <img id="imagemConvertida" style="width:200px;" />
                            <h4> Imagem </h4>
                            <video autoplay="true" id="webCamera" style="width:50%"></video>

                            <input type="text" id="base_img" name="base_img" class='d-none' />
                            <input type="text" id="credencial_img" name="credencial_img" class='d-none' />
                            <button type="button" onclick="takeSnapShot()">Tirar foto e salvar</button>

                            <p id="caminhoImagem" class="caminho-imagem d-none"><a href="" target="_blank"></a></p>
                        </div>
                        <div class="alert alert-success  col-md-9 d-none" id="sucesso" role="alert" style="float:left;">
                        </div>
                        <div id="boxFoto" class="col-md-3 d-none" style="float:right;">
                            <img id="imgCredencial" style="width:200px;" />
                            <div class="">Clique na foto para apagar</div>
                        </div>

                    </div>
                    <div class="alert alert-danger d-none" style="background-color:red;" role="alert" id='erro'>
                        CREDENCIAL N�O ENCONTRADA!<br>
                    </div>

                </form>
                <button onclick="loadCamera()" class="cute-button d-none" id="vercam">Ver</button>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/jquery-3.4.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        $(function() {

            var removeImage = function() {
                if (confirm('Deseja apagar a foto?')) {
                var imgc = $("#imgCredencial").attr('src');

                var urlAjax = "ajax_remove_image.php?img=" + imgc;
                console.log(urlAjax);
                $.ajax({
                    type: "GET",
                    url: urlAjax,
                    dataType: "html",
                    success: function(data) {
                        alert("Imagem removida com sucesso!");
                        location.reload();
                    }
                });
                }

            };


            $("#imgCredencial").on("click", removeImage);

            var enviaCredencial = (function(credencial, feira) {
                var urlAjax = "ajax_grava_checkinExpo.php?c=" + credencial + "&f=" + feira;
                console.log(urlAjax);
                $.ajax({
                    type: "GET",
                    url: urlAjax,
                    dataType: "html",
                    success: function(data) {
                        $('#carregando').css('display','none');
                        if (data == "1") {
                            $("#imgCredencial").attr("src", "");
                            $("#imagemConvertida").attr("src", "");
                            $("#image-div").addClass("d-none");
                            $("#sucesso").addClass("d-none");
                            $("#erro").removeClass("d-none");
                            $("#erro").append("CREDENCIAL N�O ENCONTRADA!<br><strong style='color:#000;'> ACESSO N�O PERMITIDO </strong>");
                            $("#credencial").val("");
                            return false;
                        }
                        if (data == "2") {
                            $("#imgCredencial").attr("src", "");
                            $("#imagemConvertida").attr("src", "");
                            $("#image-div").addClass("d-none");
                            $("#sucesso").addClass("d-none");
                            $("#erro").removeClass("d-none");
                            $("#erro").append("CREDENCIAL CANCELADA!<br><strong style='color:#000;'> ACESSO N�O PERMITIDO </strong>");
                            $("#credencial").val("");
                            return false;
                        }
                        if (data == "3") {
                            $("#imgCredencial").attr("src", "");
                            $("#imagemConvertida").attr("src", "");
                            $("#image-div").addClass("d-none");
                            $("#sucesso").addClass("d-none");
                            $("#erro").removeClass("d-none");
                            $("#erro").append("N�O FOI POSS�VEL CONSULTAR PEND�NCIAS DIRIJA-SE AO CAEX!<br><strong style='color:#000;'> ACESSO N�O PERMITIDO </strong>");
                            $("#credencial").val("");
                            return false;
                        }
                        if (data == "4") {
                            $("#imgCredencial").attr("src", "");
                            $("#imagemConvertida").attr("src", "");
                            $("#image-div").addClass("d-none");
                            $("#sucesso").addClass("d-none");
                            $("#erro").removeClass("d-none");
                            $("#erro").append("SEM CONTRATO ATIVO!<br><strong style='color:#000;'> ACESSO N�O PERMITIDO </strong>");
                            $("#credencial").val("");
                            return false;
                        }
                        if (data == "5") {
                            $("#imgCredencial").attr("src", "");
                            $("#imagemConvertida").attr("src", "");
                            $("#image-div").addClass("d-none");
                            $("#sucesso").addClass("d-none");
                            $("#erro").removeClass("d-none");
                            $("#erro").append("PEND�NCIA FINANCEIRA!<br><strong style='color:#000;'> ACESSO N�O PERMITIDO </strong>");
                            $("#credencial").val("");
                            return false;
                        }
                        if (data == "6") {
                            $("#imgCredencial").attr("src", "");
                            $("#imagemConvertida").attr("src", "");
                            $("#image-div").addClass("d-none");
                            $("#sucesso").addClass("d-none");
                            $("#erro").removeClass("d-none");
                            $("#erro").append("MONTADOR DESATIVADO!<br><strong style='color:#000;'> ACESSO N�O PERMITIDO </strong>");
                            $("#credencial").val("");
                            return false;
                        }
                        if (data == "7") {
                            $("#imgCredencial").attr("src", "");
                            $("#imagemConvertida").attr("src", "");
                            $("#image-div").addClass("d-none");
                            $("#sucesso").addClass("d-none");
                            $("#erro").removeClass("d-none");
                            $("#erro").append("PEDIDO N�O ENCONTRADO DIRIJA-SE AO CAEX!<br><strong style='color:#000;'> ACESSO N�O PERMITIDO </strong>");
                            $("#credencial").val("");
                            return false;
                        }
                        if (data == "8") {
                            $("#imgCredencial").attr("src", "");
                            $("#imagemConvertida").attr("src", "");
                            $("#image-div").addClass("d-none");
                            $("#sucesso").addClass("d-none");
                            $("#erro").removeClass("d-none");
                            $("#erro").append("PEND�NCIA FINANCEIRA!<br><strong style='color:#000;'> ACESSO N�O PERMITIDO </strong>");
                            $("#credencial").val("");
                            return false;
                        }

                        if (data !== "") {
                            $("#imagemConvertida").attr("src", "");
                            $("#image-div").addClass("d-none");
                            $("#erro").addClass("d-none");
                            $("#sucesso").removeClass("d-none");
                            $("#sucesso").append(data);
                            console.log(data);
                            $("#credencial").val("");

                            $("#credencial_img").val("");
                            $("#credencial_img").val(credencial);

                            var urlimage = "fotos/" + feira + "_" + credencial + ".jpg";
                            console.log(urlimage);
                            

                            $.ajax({
                                url: urlimage,
                                type: 'HEAD',
                                success: function() {

                                    $("#imgCredencial").attr("src", "");
                                    $("#imagemConvertida").attr("src", "");
                                    $("#imgCredencial").attr("src", urlimage);
                                    $("#boxFoto").removeClass("d-none")

                                },
                                error: function() {

                                    $("#imgCredencial").attr("src", "");
                                    $("#imagemConvertida").attr("src", "");
                                    $("#image-div").removeClass("d-none")
                                    $("#vercam").trigger("click");
                                },
                            });

                            return false;
                        }

                    }
                });
            })

            //CONSULTA CREDENCIAL
            $("#credencial").on("keypress", function(event) {
                if (event.which == 13) {
                    //cancela a a��o padr�o
                    event.preventDefault();

                    var credencial = $("#credencial").val();
                    var idFeira = $("#idFeira").val();
                    //$('#carregando').removeClass("d-none");
                    if ($(this).val().length == 7) {
                        $('#sucesso').html("");
                        $('#erro').html("");
                        enviaCredencial(credencial, idFeira);
                    } else {
                        $('#sucesso').html("");
                        $('#erro').html("");
                        $("#sucesso").addClass("d-none");
                        $("#erro").removeClass("d-none");
                        $("#erro").append("<strong>ESTA CREDENCIAL N�O � V�LIDA!<br>ACESSO N�O PERMITIDO</strong>");
                        $("#credencial").val("");
                        $('#carregando').css('display','none');
                        $("#boxFoto").addClass("d-none")
                    }
                }

            });

            $("#credencial").on('input', function() {
                let tamanho = $(this).val();

                if (tamanho.length == 7) {
                    event.preventDefault();

                    $('#sucesso').html("");
                    $('#erro').html("");
                    $("#sucesso").addClass("d-none");
                    $("#erro").addClass("d-none");
                    $("#boxFoto").addClass("d-none")
                    
                    $('#carregando').css('display','inline');
                    
                    console.log('auto envia....');

                    //sleep(2);
                    var credencial = $("#credencial").val();
                    var idFeira = $("#idFeira").val();

                    if ($(this).val().length == 7) {
                        $('#sucesso').html("");
                        $('#erro').html("");
                        enviaCredencial(credencial, idFeira);
                       // $('#boxFoto').removeClass("d-none");
                        //$('#carregando').css('display','none');
                    } else {
                        $('#sucesso').html("");
                        $('#erro').html("");
                        $("#sucesso").addClass("d-none");
                        $("#erro").removeClass("d-none");
                        $("#erro").append("<strong>ESTA CREDENCIAL N�O � V�LIDA!<br>ACESSO N�O PERMITIDO</strong>");
                        $("#credencial").val("");
                        $('#carregando').css('display','none');
                        //$('#carregando').addClass("d-none");
                    }
                }
                console.log(tamanho.length);
            });

            // tecla de atalho para tirar foto
            $(document).on('keypress', function(e) {
                //console.log(e.which);
                if (e.which == 122) {
                    console.log('You pressed SNAPSHOT!');
                    takeSnapShot()
                }
            });
        });
    </script>
</body>

</html>